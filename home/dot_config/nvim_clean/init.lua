-- https://github.com/folke/lazy.nvim#-installation
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- https://github.com/folke/lazy.nvim#-structuring-your-plugins
require("lazy").setup("plugins")

vim.cmd[[colorscheme gruvbox-material]]
-- Gitsigns.blame_line is barely readable with gruvbox
-- vim.cmd[[colorscheme gruvbox]]
-- vim.cmd[[colorscheme tokyonight]]
