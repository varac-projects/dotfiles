-- for other filetype stuff see legacy ./ftdetect/filetypes.vim
-- https://neovim.io/doc/user/lua.html#vim.filetype
vim.filetype.add({
  extension = {
    tf = "terraform",
    tfvars = "terraform",
    tfstate = "json",
    tofu = "terraform",
  },
  filename = {
    [".markdownlintrc"] = "jsonc",
  },
})
