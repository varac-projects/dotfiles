return {
  -- https://github.com/ziontee113/icon-picker.nvim#installation
  "ziontee113/icon-picker.nvim",
  config = function()
    require("icon-picker").setup({
      disable_legacy_commands = true,
    })
  end,
  keys = {
    { "<leader>in", "<cmd>IconPickerNormal<cr>", desc = "Icon Picker in normal mode" },
    { "<leader>iy", "<cmd>IconPickerYank<cr>", desc = "Icon Picker in yank mode" },
    { "<leader>ii", "<cmd>IconPickerInsert<cr>", desc = "Icon Picker in insert mode" },
  },
}
