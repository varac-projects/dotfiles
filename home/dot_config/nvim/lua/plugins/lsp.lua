-- TODO: Continue with https://www.arthurkoziel.com/json-schemas-in-neovim/
-- * Add schema to lualine (doesn't work yet)

local util = require("lspconfig.util")
return {
  {
    -- https://github.com/someone-stole-my-name/yaml-companion.nvim
    "someone-stole-my-name/yaml-companion.nvim",
    -- Disabled because Lazyvim v14 removed Telescope by default
    -- config = function()
    --   require("telescope").load_extension("yaml_schema")
    -- end,
  },
  {
    -- Doesn't seem to work ?
    -- https://github.com/soulis-1256/hoverhints.nvim#installation
    "soulis-1256/hoverhints.nvim",
  },
  {
    -- https://www.lazyvim.org/plugins/lsp
    -- https://github.com/neovim/nvim-lspconfig
    "neovim/nvim-lspconfig",
    dependencies = { "yaml-companion.nvim" },
    ---@class PluginLspOpts
    opts = {
      -- type def disabled
      -- -@type lspconfig.options
      format_notify = true,
      servers = {
        -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md

        -- https://www.npmjs.com/package/@ansible/ansible-language-server?activeTab=versions
        -- https://ansible.readthedocs.io/projects/vscode-ansible/als/
        -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#ansiblels
        -- TODO: Re-add later when installation is fixed and a new version >
        -- Doesn't install at the moment!
        -- See https://github.com/ansible/vscode-ansible/issues/1134 and
        -- https://github.com/AstroNvim/astrocommunity/issues/894#issuecomment-2065091662
        -- ansiblels = {},
        bashls = {},
        beancount = {
          -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/configs.md#beancount
          -- https://github.com/polarmutex/beancount-language-server#neovim
          -- - [AUR package](https://aur.archlinux.org/packages/beancount-language-server#comment-944608)
          --   is still outdated (1.3.1)
          --   See [mason-registry](https://github.com/mason-org/mason-registry/blob/main/packages/beancount-language-server/package.yaml)
          -- - Therefore beancount-language-server is installed manually (eget polarmutex/beancount-language-server)
          --
          -- Issues:
          --   - [Completions don't work](https://github.com/polarmutex/beancount-language-server/issues/477)
          --   - Can't disable "Flagged" warning
          -- cmd = {
          --   "/home/varac/bin/download/beancount-language-server-1.3.6",
          --   "--stdio",
          -- },
          init_options = {
            journal_file = "/home/varac/beancount/personal.beancount",
          },
        },
        cssls = {},
        -- https://github.com/microsoft/compose-language-service
        -- Todo: Don't start on arbitrary yaml files
        -- Also, docker_compose_language_service does reorganise
        -- long lines
        docker_compose_language_service = {
          filetypes = { "yaml.docker-compose" },
        },
        dockerls = {},
        eslint = {},
        -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#html
        html = {
          init_options = {
            -- Disable because it screwed up my website badly
            provideFormatter = false,
          },
        },
        -- Doesn't install ... ?
        -- java_language_server = {},
        jsonls = {
          settings = {
            -- https://www.arthurkoziel.com/json-schemas-in-neovim/
            json = {
              schemas = require("schemastore").json.schemas({
                select = {
                  -- Schema names from https://www.schemastore.org
                  "Renovate",
                  "GitHub Workflow Template Properties",
                },
              }),
              validate = { enable = true },
            },
          },
        },
        -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#jsonnet_ls
        jsonnet_ls = {},
        -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/configs.md#ltex
        ltex = {
          filetypes = { "gitcommit", "markdown", "md", "rst", "rmd", "text" },
          flags = { debounce_text_changes = 300 },
          -- https://valentjn.github.io/ltex/settings-de.html#ltexlanguage
          -- https://www.reddit.com/r/neovim/comments/z3uxjd/comment/ixz7m1m/
          settings = {
            ltex = {
              -- https://valentjn.github.io/ltex/settings.html
              --
              -- When using the language code "auto", LTEX will try to detect
              -- the language of the document. This is not recommended,
              -- as only generic languages like "en" or "de" will be detected
              -- and thus no spelling errors will be reported.
              language = "auto",
              -- language = "de-DE",
              disabledRules = {
                ["en"] = { "CURRENCY" },
                ["de"] = { "DE_CASE", "MATHE", "VON_BIS", "GERMAN_WORD_REPEAT_RULE" },
              },
            },
          },
        },
        marksman = {},
        -- https://github.com/eclipse/lemminx
        -- XML Language Server
        lemminx = {},
        -- TOML ls
        taplo = {
          settings = {
            -- https://www.arthurkoziel.com/json-schemas-in-neovim/
            evenBetterToml = {
              schema = {
                -- add additional schemas
                associations = {
                  ["example\\.toml$"] = "https://json.schemastore.org/example.json",
                },
              },
            },
          },
        },
        -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/configs.md#terraformls
        terraformls = {},
        tflint = {},
        -- https://github.com/redhat-developer/yaml-language-server
        -- https://github.com/someone-stole-my-name/yaml-companion.nvim
        yamlls = require("yaml-companion").setup({
          -- https://github.com/redhat-developer/yaml-language-server?tab=readme-ov-file#language-server-settings
          -- detect k8s schemas based on file content
          builtin_matchers = {
            kubernetes = { enabled = true },
          },

          -- schemas available in Telescope picker
          schemas = {

            -- not loaded automatically, manually select with
            -- :Telescope yaml_schema
            -- {
            --   name = "Argo CD Application",
            --   uri = "https://raw.githubusercontent.com/datreeio/CRDs-catalog/main/argoproj.io/application_v1alpha1.json",
            -- },
            -- {
            --   name = "SealedSecret",
            --   uri = "https://raw.githubusercontent.com/datreeio/CRDs-catalog/main/bitnami.com/sealedsecret_v1alpha1.json",
            -- },

            -- Extra schemas not available in the schemaStore
            --
            -- Flux2
            -- https://github.com/JJGadgets/flux2-schemas
            {
              name = "Flux HelmRelease",
              uri = "https://raw.githubusercontent.com/fluxcd-community/flux2-schemas/main/helmrelease-helm-v2beta2.json",
            },
            {
              name = "Flux Kustomization",
              uri = "https://raw.githubusercontent.com/fluxcd-community/flux2-schemas/main/kustomization-kustomize-v1.json",
            },
            {
              name = "Flux HelmRepository",
              uri = "https://raw.githubusercontent.com/fluxcd-community/flux2-schemas/main/helmrepository-source-v1beta2.json",
            },
            {
              name = "Flux GitRepository",
              uri = "https://raw.githubusercontent.com/fluxcd-community/flux2-schemas/main/gitrepository-source-v1.json",
            },

            -- schemas below are automatically loaded, but added
            -- them here so that they show up in the statusline
            {
              name = "Kustomization",
              uri = "https://json.schemastore.org/kustomization.json",
            },
            {
              name = "GitHub Workflow",
              uri = "https://json.schemastore.org/github-workflow.json",
            },
            -- Disabled in favor of https://github.com/alesbrelih/gitlab-ci-ls
            -- {
            --   name = "Gitlab CI",
            --   uri = "https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json",
            -- },
            {
              -- Doesn't seem to detect invalid keys ?
              name = "Helmfile",
              uri = "https://json.schemastore.org/helmfile.json",
            },
          },
          lspconfig = {
            settings = {
              yaml = {
                validate = true,
                customTags = {
                  -- For .gitlab-ci.yaml
                  -- https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#configure-your-ide-to-support-reference-tags
                  "!reference sequence",
                },
                -- https://www.reddit.com/r/neovim/comments/ze9gbe/kubernetes_auto_completion_support_in_neovim/
                -- https://github.com/redhat-developer/yaml-language-server?tab=readme-ov-file#more-examples-of-schema-association
                -- https://www.arthurkoziel.com/json-schemas-in-neovim/
                schemaStore = {
                  enable = false,
                  url = "",
                },
                schemas = require("schemastore").yaml.schemas({
                  -- From https://www.arthurkoziel.com/json-schemas-in-neovim/:
                  -- "Changing the filename matching pattern is not possible when
                  -- using select but can be done by replacing a schema
                  -- (check the docs for the replace function) which allows to
                  -- set a new fileMatch attribute."
                  select = {
                    "kustomization.yaml",
                    "GitHub Workflow",
                    "gitlab-ci",
                  },
                  -- extra schemas won't show up in the yaml-companion telescope
                  -- picker, and also won't show up in the Luabar
                  extra = {},
                }),
              },
            },
          },
        }),
      },
    },
  },
}
