return {
  {
    -- https://www.lazyvim.org/plugins/treesitter#nvim-treesitter
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      -- add language parsers to the
      -- [LazyVim default list](https://www.lazyvim.org/plugins/treesitter)
      -- https://github.com/nvim-treesitter/nvim-treesitter?tab=readme-ov-file#supported-languages
      vim.list_extend(opts.ensure_installed, {
        "beancount",
        "css",
        "ini",
        "sql",
      })
    end,
  },
  {
    -- https://github.com/JoosepAlviste/nvim-ts-context-commentstring#getting-started
    -- https://github.com/LazyVim/LazyVim/issues/1368#issuecomment-1735338404
    "JoosepAlviste/nvim-ts-context-commentstring",
    opts = {
      config = {
        beancount = "; %s",
        markdown = "> %s",
      },
    },
  },
}
