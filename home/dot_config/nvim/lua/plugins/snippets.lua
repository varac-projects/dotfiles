-- https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/plugins/coding.lua#L5
-- https://github.com/LazyVim/starter/blob/main/lua/plugins/example.lua#L217
return {
  -- https://github.com/L3MON4D3/LuaSnip#setup
  "L3MON4D3/LuaSnip",
  dependencies = {
    "rafamadriz/friendly-snippets",
    config = function()
      -- Use existing VS Code style snippets from a plugin
      -- (eg. rafamadriz/friendly-snippets)
      require("luasnip.loaders.from_vscode").lazy_load()
      -- Include custom snippets from ~/.config/nvim/snippets/
      require("luasnip.loaders.from_snipmate").lazy_load()
    end,
  },
}
