return {
  {
    "williamboman/mason.nvim",
    opts = {
      -- Test local development of the registry
      -- registries = {
      --   "file:~/projects/neovim/mason-registry",
      -- },
      -- log_level = vim.log.levels.DEBUG,
      ensure_installed = {
        "markdownlint",
        "markdown-toc",
        "trivy",
      },
    },
  },
  {
    "williamboman/mason-lspconfig.nvim",
    opts = {
      -- Doesn't seem to have any effect :(
      -- https://github.com/williamboman/mason-lspconfig.nvim?tab=readme-ov-file#configuration
      -- automatic_installation = { exclude = { "ansiblels" } },
    },
  },
}
