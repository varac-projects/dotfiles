-- https://www.lazyvim.org/plugins/ui#lualinenvim
-- https://www.lazyvim.org/configuration/examples

return {
  {
    -- https://www.lazyvim.org/plugins/ui#lualinenvim
    -- https://github.com/nvim-lualine/lualine.nvim
    "nvim-lualine/lualine.nvim",
    event = "VeryLazy",
    opts = function(_, opts)
      -- get yaml-companion schema for current buffer
      local get_schema = function()
        local schema = require("yaml-companion").get_buf_schema(0)
        if schema.result[1].name == "none" then
          return [[no json schema]]
        end
        return schema.result[1].name
      end
      -- Get active nvim-lint linters
      -- https://github.com/mfussenegger/nvim-lint?tab=readme-ov-file#get-the-current-running-linters-for-your-buffer
      local lint_progress = function()
        local linters = require("lint").get_running()
        if #linters == 0 then
          return "󰦕"
        end
        return "󱉶 " .. table.concat(linters, ", ")
      end
      -- Show filetype as text as well, not only as icon
      table.insert(opts.sections.lualine_c, { "filetype", icon_only = false })
      table.insert(opts.sections.lualine_c, get_schema)
      table.insert(opts.sections.lualine_c, lint_progress)

      -- Remove date from section z
      table.remove(opts.sections.lualine_z, 1)
      -- Set current spelllang in buffer
      table.insert(opts.sections.lualine_z, "bo:spelllang")
    end,
  },
}
