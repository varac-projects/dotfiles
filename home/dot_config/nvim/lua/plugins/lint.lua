return {
  {
    "mfussenegger/nvim-lint",
    -- TODO: Find a way to show configured linters for current
    --       filetype
    opts = function(_, opts)
      table.insert(opts.linters_by_ft.dockerfile, "trivy")
      -- Replace markdownlint-cli2 with markdownlint-cli
      -- which searches config files in parent directories
      table.remove(opts.linters_by_ft.markdown)
      table.insert(opts.linters_by_ft.markdown, "markdownlint")
      -- lazyvim nvim-lint defaults doesn't contain any linter
      -- for the `sh` filetype, so we can't simply insert anything
      -- to a non-existing key, but rather need to add it.
      -- shellcheck is already configured as linter
      -- table.insert(opts.linters_by_ft, { sh = "shellcheck" })
    end,
  },
}
