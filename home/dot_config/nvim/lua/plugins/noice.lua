return {
  {
    -- https://www.lazyvim.org/plugins/ui#noicenvim
    "folke/noice.nvim",
    opts = {
      lsp = {
        progress = {
          -- https://www.reddit.com/r/neovim/comments/11au027/comment/j9xd1m4/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button
          enabled = false,
        },
      },
    },
  },
}
