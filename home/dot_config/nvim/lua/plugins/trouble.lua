return {
  "folke/trouble.nvim",
  -- opts will be merged with the parent spec
  opts = {
    -- -- https://github.com/folke/trouble.nvim
    mode = "document_diagnostics",
    -- wrap lines in trouble buffer
    -- https://github.com/folke/trouble.nvim/pull/326#issuecomment-2307625092
    win = {
      wo = {
        wrap = true,
      },
    },
    -- use_diagnostic_signs = true,
    -- auto_open = true,
    -- auto_close = true,
  },
}
