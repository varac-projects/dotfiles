return {
  -- https://wakatime.com/neovim#lazyvim-install
  { "wakatime/vim-wakatime", lazy = false },
  --
  -- https://github.com/TobinPalmer/Tip.nvim
  -- {
  --   "TobinPalmer/Tip.nvim",
  --   event = "VimEnter",
  --   init = function()
  --     --- @type Tip.config
  --     require("tip").setup({
  --       title = "Tip!",
  --       url = "https://vtip.43z.one",
  --     })
  --   end,
  -- },
  {
    -- https://github.com/jghauser/follow-md-links.nvim
    -- Alternatives: See ~/wiki/doc/documentation/knowledge-base-apps.md
    "jghauser/follow-md-links.nvim",
  },
  {
    -- https://github.com/folke/twilight.nvim
    "folke/twilight.nvim",
  },
  {
    -- https://github.com/folke/zen-mode.nvim
    "folke/zen-mode.nvim",
  },
  {
    -- https://github.com/Konfekt/vim-DetectSpellLang
    "Konfekt/vim-DetectSpellLang",
  },
  {
    -- https://github.com/someone-stole-my-name/yaml-companion.nvim
    "someone-stole-my-name/yaml-companion.nvim",
  },
  -- TODO: Cant make it work
  -- {
  --   -- https://github.com/aliqyan-21/wit.nvim
  --   "https://github.com/aliqyan-21/wit.nvim",
  --   config = function()
  --     require("wit").setup()
  --   end,
  --   lazy = false,
  --   keys = {
  --     -- tylua: ignore
  --     {
  --       "<leader>sB",
  --       function()
  --         require("wit").command_search_visual()
  --         -- require("wit").WitSearchVisual()
  --       end,
  --       desc = "Search selection using Browser",
  --       mode = { "v" },
  --     },
  --   },
  -- },
  {
    -- https://github.com/topaxi/pipeline.nvim
    -- Disabled because of https://github.com/topaxi/pipeline.nvim/issues/21
    --   "topaxi/pipeline.nvim",
    --   keys = {
    --     { "<leader>ci", "<cmd>Pipeline<cr>", desc = "Open pipeline.nvim" },
    --   },
    --   -- https://github.com/topaxi/pipeline.nvim?tab=readme-ov-file#options
    --   opts = {
    --     browser = "xdg-open",
    --     allowed_hosts = {
    --       "gitlab.com",
    --     },
    --   },
  },
  {
    -- https://github.com/miversen33/netman.nvim/wiki/User-Guide#lazy
    "miversen33/netman.nvim",
    dependencies = {
      "nvim-neo-tree/neo-tree.nvim",
      init = function()
        require("neo-tree").setup({
          sources = { "filesystem", "buffers", "git_status", "netman.ui.neo-tree" },
          source_selector = {
            sources = {
              { source = "filesystem" },
              { source = "buffers" },
              { source = "git_status" },
              { source = "remote" },
            },
          },
        })
      end,
    },
  },
}
