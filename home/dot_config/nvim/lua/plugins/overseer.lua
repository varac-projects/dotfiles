return {
  {
    "stevearc/overseer.nvim",
    opts = {
      -- https://github.com/stevearc/overseer.nvim/blob/master/doc/third_party.md#toggleterm
      -- https://github.com/akinsho/toggleterm.nvim?tab=readme-ov-file#setup
      -- Can't make the WatchRun task work with toggleterm
      -- https://github.com/stevearc/overseer.nvim/discussions/369
      -- strategy = {
      --   "toggleterm",
      --   -- use_shell = true
      -- },
      -- https://github.com/stevearc/overseer.nvim/blob/master/doc/tutorials.md#run-a-file-on-save
      templates = { "builtin", "user.run_script" },
    },
    -- https://github.com/stevearc/overseer.nvim/blob/master/doc/tutorials.md#run-a-file-on-save
    init = function()
      vim.api.nvim_create_user_command("WatchRun", function()
        local overseer = require("overseer")
        overseer.run_template({ name = "run script" }, function(task)
          if task then
            -- https://github.com/stevearc/overseer.nvim/blob/master/doc/components.md?plain=1
            task:add_component({ "restart_on_save", paths = { vim.fn.expand("%:p") } })
            local main_win = vim.api.nvim_get_current_win()
            overseer.run_action(task, "open hsplit")
            vim.api.nvim_set_current_win(main_win)
          else
            vim.notify("WatchRun not supported for filetype " .. vim.bo.filetype, vim.log.levels.ERROR)
          end
        end)
      end, {})
    end,
  },
}
