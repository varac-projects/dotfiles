return {
  "akinsho/toggleterm.nvim",
  version = "*",
  opts = {},
  keys = {
    {
      "<leader>td",
      "<cmd>ToggleTerm size=40 dir=~/Desktop direction=horizontal<cr>",
      desc = "Open a horizontal ToggleTerm terminal at the Desktop directory",
    },
  },
}
