-- https://www.lazyvim.org/plugins/colorscheme
-- [Treesitter compatible colorschemes](https://github.com/nvim-treesitter/nvim-treesitter/wiki/Colorschemes)
return {

  -- https://github.com/sainnhe/gruvbox-material
  -- vimscript + lua
  -- { "sainnhe/gruvbox-material" },
  -- https://github.com/ellisonleao/gruvbox.nvim
  -- A port of gruvbox community theme to lua with treesitter support!
  -- lua port

  { "ellisonleao/gruvbox.nvim", priority = 1000, config = true, opts = {} },
  -- luisiacc/gruvbox-baby
  -- { "luisiacc/gruvbox-baby" },

  -- https://github.com/sainnhe/everforest
  -- { "sainnhe/everforest" },

  -- https://github.com/craftzdog/solarized-osaka.nvim
  -- { "craftzdog/solarized-osaka.nvim", lazy = false, priority = 1000, opts = {} },

  -- Actually use the specified colorscheme
  {
    "LazyVim/LazyVim",
    opts = {
      -- Gitsigns.blame_line is barely readable with gruvbox
      -- catppuccin-latte, catppuccin-frappe, catppuccin-macchiato, catppuccin-mocha
      -- colorscheme = "catppuccin-mocha",
      -- colorscheme = "gruvbox-material",
      colorscheme = "gruvbox",
      -- colorscheme = "solarized-osaka",
    },
  },
}
