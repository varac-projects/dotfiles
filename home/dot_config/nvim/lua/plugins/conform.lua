return {
  {
    -- https://github.com/stevearc/conform.nvim?tab=readme-ov-file#setup
    -- https://www.lazyvim.org/plugins/formatting#conformnvim
    "stevearc/conform.nvim",
    opts = function()
      local plugin = require("lazy.core.config").plugins["conform.nvim"]
      local opts = {
        -- /home/varac/.local/state/nvim/conform.log
        log_level = vim.log.levels.DEBUG,
        default_format_opts = {
          timeout_ms = 3000,
          async = false,
          quiet = false,
        },
        -- The options you set here will be merged with the builtin formatters.
        -- You can also define any custom formatters here.
        formatters = {
          injected = { options = { ignore_errors = true } },
          beanhub_cli_format = {
            command = "bh",
            args = { "format", "--stdin-mode" },
            stdin = true,
          },
          markdownlint = {},
          ["markdown-toc"] = {
            args = { "--bullets", "-", "$FILENAME" },
          },
          -- https://dev.to/miry/effortless-formatting-for-opentofu-files-with-lazyvim-4d10
          tofu_fmt = {
            command = "tofu",
            args = { "fmt", "-" },
            stdin = true,
          },
        },
        formatters_by_ft = {
          beancount = { "beanhub_cli_format" },
          css = { "prettier" },
          json = { "prettier" },
          jsonc = { "prettier" },
          lua = { "stylua" },
          -- mdformat is too unflexible, cannot diable rules and is missing
          -- a plugin for formatting slides (seperators). Therefore the default
          -- lazyvim markdown formatters are used again
          -- https://www.lazyvim.org/extras/lang/markdown#conformnvim-optional
          -- Replace markdownlint-cli2 with markdownlint-cli
          -- which searches config files in parent directories
          markdown = { "prettier", "markdownlint", "markdown-toc" },
          -- MDX lets you use JSX in your markdown content
          -- https://mdxjs.com/
          ["markdown.mdx"] = { "prettier", "markdownlint-cli", "markdown-toc" },
          -- https://docs.astral.sh/ruff/faq/#is-the-ruff-linter-compatible-with-black
          -- Black is only added to fix long lines, with `--preview` enabled in
          -- `~/pyproject.toml`, because I can't make it work with `ruff format`
          python = { "black", "ruff_format" },
          -- beautysh: last commit 2023-04, defaults to 4 spaces for indentation
          -- shellharden:
          sh = { "shfmt", "shellcheck" },
          -- https://github.com/tamasfe/taplo
          toml = { "taplo" },
          -- https://www.lazyvim.org/extras/lang/terraform#conformnvim-optional
          terraform = { "tofu_fmt" },
          tf = { "tofu_fmt" },
          ["terraform-vars"] = { "tofu_fmt" },
          -- yamlfix and yamlfmt are both in maintenance mode / slow
          yaml = { "prettier" },
        },
        -- Lazyvim complains that one should not override format_on_save()
        --
        -- format_on_save = function(bufnr)
        --   -- Disable with a global or buffer-local variable
        --   if vim.g.disable_autoformat or vim.b[bufnr].disable_autoformat then
        --     return
        --   end
        --   return { timeout_ms = 500, lsp_format = "fallback" }
        -- end,
      }
      return opts
    end,
  },
}
