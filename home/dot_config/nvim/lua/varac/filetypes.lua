-- TODO: Migrate rest of ~/.config/nvim/ftdetect/filetypes.vim
-- Old file: ~/.config/nvim.bak/ftdetect/filetypes.vim
-- Taken from https://github.com/Jarmos-san/dotfiles/blob/main/dotfiles/.config/nvim/filetype.lua
vim.filetype.add({
  -- Detect and assign filetype based on the extension of the filename
  extension = {
    astro = "astro",
    conf = "conf",
    -- sh.dotenv ensures shell syntax highlighting AND dotenv-linter support
    env = "sh.dotenv",
    -- https://github.com/pfeiferj/tree-sitter-hurl
    hurl = "hurl",
    log = "log",
    mdx = "mdx",
  },
  -- Detect and apply filetypes based on the entire filename
  filename = {
    ["docker-compose.yaml"] = "yaml.docker-compose",
    ["docker-compose.yml"] = "yaml.docker-compose",
    [".env.local"] = "sh.dotenv",
    -- https://github.com/alesbrelih/gitlab-ci-ls?tab=readme-ov-file#integration-with-neovim
    [".gitlab*"] = "yaml.gitlab",
    ["goss.yaml"] = "gotmpl",
    ["helmfile.yaml"] = "yaml",
    [".markdownlintrc"] = "json",
    ["tsconfig.json"] = "jsonc",
  },
  -- Detect and apply filetypes based on certain patterns of the filenames
  pattern = {
    -- INFO: Match filenames like - ".env.example", ".env.local" and so on
    ["%.env%.[%w_.-]+"] = "dotenv",
  },
})
