-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

-- Various LazyVim options
-- http://www.lazyvim.org/extras/lang/python#options
vim.g.lazyvim_python_lsp = "basedpyright"
