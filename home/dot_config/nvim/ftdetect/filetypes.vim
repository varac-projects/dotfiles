" For detecting filetypes see ../filetype.lua
au BufRead,BufNewFile */ansible/*.yml if expand("<afile>") !~ '\/files\/\|docker-compose\.yml\|docker-compose\.yaml' | set filetype=yaml.ansible | endif

" Set spell to all text documents, to enable vim-DetectSpellLang
" https://github.com/Konfekt/vim-DetectSpellLang?tab=readme-ov-file#set-up
autocmd FileType text,markdown,mail setlocal spell
