-- https://muniftanjim.dev/blog/neovim-project-local-config-with-exrc-nvim/
-- loads local .exrc, .nvimrc and .nvim.lua files
-- vim.o.exrc = true

-- https://github.com/Konfekt/vim-DetectSpellLang
vim.g.detectspelllang_langs = {
  hunspell = { "en_US", "de_DE" },
  aspell = { "en_US", "de_DE" },
}
vim.g.detectspelllang_program = "hunspell"

-- Load lua/config/*
require("config.lazy")

require("varac.filetypes")

-- https://bitcrowd.dev/folding-sections-of-markdown-in-vim/
vim.g.markdown_folding = 1

-- WARNING Your virtualenv is not set up optimally.
-- ADVICE: Create a virtualenv specifically for Nvim and use g:python3_host_prog.
-- This will avoid the need to install the pynvim module in each virtualenv.
-- https://neovim.io/doc/user/provider.html#provider-python
vim.g.python3_host_prog = "/usr/sbin/python"

-- https://github.com/ngalaiko/tree-sitter-go-template?tab=readme-ov-file#neovim-integration-using-nvim-treesitter
local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
parser_config.gotmpl = {
  install_info = {
    url = "https://github.com/ngalaiko/tree-sitter-go-template",
    files = { "src/parser.c" },
  },
  filetype = "gotmpl",
  used_by = { "gohtmltmpl", "gotexttmpl", "gotmpl" },
}

-- Doesn't work atm
--
-- https://github.com/stevearc/conform.nvim/blob/master/doc/recipes.md#command-to-toggle-format-on-save
-- vim.api.nvim_create_user_command("FormatDisable", function(args)
--   if args.bang then
--     -- FormatDisable! will disable formatting just for this buffer
--     vim.b.disable_autoformat = true
--   else
--     vim.g.disable_autoformat = true
--   end
-- end, {
--   desc = "Disable autoformat-on-save",
--   bang = true,
-- })
-- vim.api.nvim_create_user_command("FormatEnable", function()
--   vim.b.disable_autoformat = false
--   vim.g.disable_autoformat = false
-- end, {
--   desc = "Re-enable autoformat-on-save",
-- })
