return {
  -- https://github.com/lewis6991/gitsigns.nvim
  "lewis6991/gitsigns.nvim",
  opts = {
    current_line_blame = true,
  },
}
