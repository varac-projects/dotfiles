# vim: set ft=sh

autoload -U add-zsh-hook

gopass_used_in_file() {
  if [ -f "$1" ]; then
    if grep -q gopass "$1"; then
      echo "❌ gopass is use in $1"
    fi
  fi

}

_gopass_used() {
  if [ ! -f .mise.local.gopass-check.disable ]; then
    gopass_used_in_file .mise.toml
    gopass_used_in_file .mise.local.toml
    gopass_used_in_file Taskfile.yaml
  fi
}

add-zsh-hook precmd _gopass_used
