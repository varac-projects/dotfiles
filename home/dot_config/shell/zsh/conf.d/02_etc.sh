# Set emacs line editing mode
set -o emacs

# Use vi-style keybindings
# https://dougblack.io/words/zsh-vi-mode.html
#bindkey -v
# Make Vi mode transitions faster (KEYTIMEOUT is in hundredths of a second)
#export KEYTIMEOUT=1

# https://direnv.net/
# Disabled in favor of https://mise.jdx.dev/
# eval "$(direnv hook zsh)"

# http://owen.cymru/fzf-ripgrep-navigate-with-bash-faster-than-ever-before/
# Arch
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
# Debian
[ -f /usr/share/doc/fzf/examples/key-bindings.zsh ] && source /usr/share/doc/fzf/examples/key-bindings.zsh

# OSD notificatios for commands that take more than 10s
# https://github.com/ihashacks/notifyosd.zsh
# . $HOME/projects/zsh/notifyosd.zsh/notifyosd.zsh

# https://dustri.org/b/my-zsh-configuration.html
# History
HISTFILE=~/.zsh_history     # where to store zsh config
HISTSIZE=10000              # big history
SAVEHIST=10000              # big history
setopt append_history       # append
setopt hist_ignore_all_dups # no duplicate
unsetopt hist_ignore_space  # ignore space prefixed commands
setopt hist_reduce_blanks   # trim blanks
setopt hist_verify          # show before executing history commands
setopt inc_append_history   # add commands as they are typed, don't wait until shell exit
setopt share_history        # share hist between sessions
setopt bang_hist            # !keyword

# make zsh ESC-Backspace delete components of a path or words and not the entire
# line
autoload -U select-word-style
select-word-style bash

# ctrl+left/right fix
# https://unix.stackexchange.com/questions/58870/ctrl-left-right-arrow-keys-issue
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

# https://github.com/Ramilito/kubesess#installation
# Disabled in favor of kubeswitch
# [ -f ~/.kube/kubesess/scripts/sh/kubesess.sh ] && source ~/.kube/kubesess/scripts/sh/kubesess.sh

# https://github.com/danielfoehrKn/kubeswitch/blob/master/docs/installation.md#option-2---manual-installation
[ -s /usr/bin/switch.sh ] && source /usr/bin/switch.sh
