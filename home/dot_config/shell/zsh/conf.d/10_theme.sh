#-------------------------------------------------------------------------------
# Sunrise theme for oh-my-zsh by Adam Lindberg (eproxus@gmail.com)
# Intended to be used with Solarized: http://ethanschoonover.com/solarized
# (Needs Git plugin for current_branch method)
#-------------------------------------------------------------------------------

# Color shortcuts
R=$fg[red]
G=$fg[green]
M=$fg[magenta]
RB=$fg_bold[red]
YB=$fg_bold[yellow]
BB=$fg_bold[blue]
RESET=$reset_color

if [ "$(whoami)" = "root" ]; then
    PROMPTCOLOR="%{$RB%}" PREFIX="-!-";
else
    #PROMPTCOLOR="" PREFIX="⏻";
    PROMPTCOLOR="" PREFIX="";
fi

local return_code="%(?..%{$R%}%?%{$RESET%})"

# get the name of the branch we are on (copied and modified from git.zsh)
function custom_git_prompt() {
  echo "$(git-radar --zsh --fetch) "
}

function rvm_info {
  rvm=$(~/.rvm/bin/rvm-prompt)
  #[ -z "$rvm" ] || echo "[$rvm] "
  [ -z "$rvm" ] || echo "[%{$fg[white]%}%{$fg[red]%}$rvm%{$fg[white]%}%{$reset_color%}] "
}

# https://medium.com/google-cloud/tools-that-make-my-life-easier-to-work-with-kubernetes-fce3801086c0
function kubecontext() {
  echo "☸ `kubectl config current-context`/`kubectl config get-contexts --no-headers | grep '*' | awk '{print $5}'` "
}

# %B sets bold text
#PROMPT='%B$PREFIX${return_code} %B$(rvm_info)%2~ $(kubecontext)$(custom_git_prompt)%{$M%}%B»%b%{$RESET%} '
PROMPT='%B$PREFIX${return_code} %B$(rvm_info)%2~ $(custom_git_prompt)%{$M%}%B»%b%{$RESET%} '

# RPS1 = right side prompt
#RPS1="${return_code}"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$YB%}‹"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$YB%}›%{$RESET%} "

ZSH_THEME_GIT_PROMPT_DIRTY="%{$R%}*"
ZSH_THEME_GIT_PROMPT_CLEAN=""

ZSH_THEME_GIT_PROMPT_AHEAD="%{$BB%}➔"

ZSH_THEME_GIT_STATUS_PREFIX=" "

# Staged
ZSH_THEME_GIT_PROMPT_STAGED_ADDED="%{$G%}A"
ZSH_THEME_GIT_PROMPT_STAGED_MODIFIED="%{$G%}M"
ZSH_THEME_GIT_PROMPT_STAGED_RENAMED="%{$G%}R"
ZSH_THEME_GIT_PROMPT_STAGED_DELETED="%{$G%}D"

# Not-staged
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$R%}⁇"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$R%}M"
ZSH_THEME_GIT_PROMPT_DELETED="%{$R%}D"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$R%}UU"
