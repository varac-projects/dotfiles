# vim: set ft=sh

autoload -U add-zsh-hook

file_present() {
  if [ -f "$1" ] || [ -d "$1" ]; then
    echo "❌ $1 present "
  fi
}

_envrc_or_mise_present() {
  file_present .envrc
  file_present .envrc.local
  file_present .direnv
}
add-zsh-hook precmd _envrc_or_mise_present
