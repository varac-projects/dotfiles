# Must get sourced *after* `~/.zshrc.d/01_etc.sh` !!!!
# https://zsh.sourceforge.io/Doc/Release/Completion-System.html

# http://askql.wordpress.com/2011/01/11/zsh-writing-own-completion/
# COMPLETION SETTINGS
# add custom completion scripts
fpath=( ~/.config/shell/zsh/completion $fpath )

# Remove unneeded complettions
# fpath=("${(@)fpath:#/home/varac/.zplug/repos/robbyrussell/oh-my-zsh/functions}")
# fpath=("${(@)fpath:#/home/varac/.zplug/repos/robbyrussell/oh-my-zsh/completions}")
#logcli --completion-script-zsh fpath=("${(@)fpath:#/usr/share/zsh/functions/Calendar}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Chpwd}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Completion/AIX}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Completion/BSD}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Completion/Cygwin}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Completion/Darwin}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Completion/Mandriva}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Completion/Redhat}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Completion/Solaris}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Completion/openSUSE}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Prompts}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/TCP}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/VCS_Info}")
# fpath=("${(@)fpath:#/usr/share/zsh/functions/Zftp}")


# Speed up zsh loading time by using cached ~/.zcompdump
# https://carlosbecker.com/posts/speeding-up-zsh/
# On slow systems, checking the cached .zcompdump file to see if it must be
# regenerated adds a noticable delay to zsh startup.  This little hack restricts
# it to once a day.  It should be pasted into your own completion file.
#
# The globbing is a little complicated here:
# - '#q' is an explicit glob qualifier that makes globbing work within zsh's [[ ]] construct.
# - 'N' makes the glob pattern evaluate to nothing when it doesn't match (rather than throw a globbing error)
# - '.' matches "regular files"
# - 'mh+24' matches files (or directories or whatever) that are older than 24 hours.
#autoload -Uz compinit
#if [[ -n ~/.zcompdump(#qN.mh+24) ]]; then
#        echo "Running compinit"
#        compinit;
#else
#        echo "Running compinit -C"
#        compinit -C;
#fi;


# https://gist.github.com/ctechols/ca1035271ad134841284#gistcomment-2894219

_zpcompinit_custom() {
  setopt extendedglob local_options
  autoload -Uz compinit
  local zcd=${ZDOTDIR:-$HOME}/.zcompdump
  local zcdc="$zcd.zwc"
  # Compile the completion dump to increase startup speed, if dump is newer or doesn't exist,
  # in the background as this is doesn't affect the current session
  if [[ -f "$zcd"(#qN.m+1) ]]; then
        compinit -i -d "$zcd"
        { rm -f "$zcdc" && zcompile "$zcd" } &!
  else
        compinit -C -d "$zcd"
        { [[ ! -f "$zcdc" || "$zcd" -nt "$zcdc" ]] && rm -f "$zcdc" && zcompile "$zcd" } &!
  fi
}

# _zpcompinit_custom

# load bash autocomplete compability
#autoload bashcompinit
#bashcompinit
#source /home/varac/leap/git/leap_cli/contrib/leap.bash-completion



# show completion menu when number of options is at least 2
#zstyle ':completion:*' menu select=2

zstyle ":completion:*:descriptions" format "%B%d%b"

# http://scottlinux.com/2011/08/19/quick-intro-to-zsh-auto-complete/
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2 eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl no
zstyle ':completion:*' verbose yes

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# For debugging, see https://github.com/andsens/homeshick/issues/89#issuecomment-40905141
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*' group-name ""

# use .ssh/config for ssh hostname completion as well
# http://unix.stackexchange.com/questions/52099/how-to-append-extend-zshell-completions
zstyle -s ':completion:*:hosts' hosts _ssh_config
[[ -r ~/.ssh/config ]] && _ssh_config+=($(cat ~/.ssh/config | sed -ne 's/Host[=\t ]//p'))
zstyle ':completion:*:hosts' hosts $_ssh_config

# Source non-native zsh completions that can't be linked in `~/.zsh/completion`

# https://python-gitlab.readthedocs.io/en/stable/cli.html#enabling-shell-autocompletion
# eval "$(register-python-argcomplete gitlab)"

# https://github.com/Ramilito/kubesess#installation
# /home/varac/.kube/kubesess/scripts/sh/completion.sh:16: command not found: complete
# [ -f ~/.kube/kubesess/scripts/sh/completion.sh ] && source ~/.kube/kubesess/scripts/sh/completion.sh

# https://wiki.archlinux.org/title/zsh#Command_completion
autoload -Uz compinit
compinit

##############
# bashcompinit completions, must come *after* compinit !
##############

[ -x /usr/sbin/logcli ] && eval "$(logcli --completion-script-zsh)"

# https://github.com/minio/mc#shell-autocompletion
# Doesn't work well
# autoload -U +X bashcompinit
# bashcompinit

# Added by mc --autocompletion
autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/bin/mcli mcli
complete -o nospace -C /usr/bin/mcli mc
