# vim: set ft=sh

autoload -U add-zsh-hook

mise_exec_cache_in_file() {
  if [ -f "$1" ]; then
    if grep 'exec(command=.rbw' "$1" | grep -q -v -E '(^#|cache_key)'; then
      echo "❌ mise exec is use without chaching in $1"
    fi
  fi

}

_mise_exec_cache() {
  if [ -z "$DISABLE_MISE_EXEC_CHECK" ]; then
    mise_exec_cache_in_file .mise.toml
    mise_exec_cache_in_file .mise.local.toml
  fi
}

add-zsh-hook precmd _mise_exec_cache
