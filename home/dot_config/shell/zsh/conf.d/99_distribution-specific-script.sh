OS_ID="$(grep '^ID=' /etc/os-release | cut -d'=' -f 2)"
[ -d /home/varac/.config/shell/os-specific/zsh/interactive/"$OS_ID" ] && \
  source /home/varac/.config/shell/os-specific/zsh/interactive/"$OS_ID"/*
