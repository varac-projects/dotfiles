autoload -U add-zsh-hook

pre_commit_check() {
  [[ -f .pre-commit-config.yaml ]] && \
    [[ ! -f .git/hooks/pre-commit ]] && \
      # ~/.pre-commit-config.yaml is used for dotfiles
      [[ "$PWD" != "/home/varac" ]] && \
        pre-commit install
}

add-zsh-hook precmd pre_commit_check
