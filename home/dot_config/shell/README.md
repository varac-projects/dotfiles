# Shell startup files

Based on this excellent blogpost: https://blog.flowblok.id.au/2013-02/shell-startup-scripts.html
and this repo: https://bitbucket.org/flowblok/shell-startup/src

Aliases for non-interactive shells/scripts work in zsh (i.e. `zsh -c ll`),
but not in bash/sh (i.e. `bash -c ll`, `sh -c ll`).
This is a [know bash limitation](https://unix.stackexchange.com/questions/1496/why-doesnt-my-bash-script-recognize-aliases).


## Todo

- move non-interactive things from bash/interactive to bash/env
- move non-interactive things from zsh/interactive to zsh/env
- move non-interactive things from common/interactive to common/env
- move
  - .sh_functions/
  - .shrc.d.disabled/
