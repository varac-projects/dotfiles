# get fqdn of current branch
stackspin_ci_branch_dnsname() {

  # Use $1 as branch name. If not passed, use current branch name
  id="$1"
  if [ -z "$1" ]
  then
    # No branch name passed as parameter, use current branch name
    id=$(git rev-parse --abbrev-ref HEAD)
  fi


  #echo "Original subdomain is : $id"

  # Truncate name as stackspin cli does as well
  #
  # In stackspin/__main__.py the truncation logic is:
  #
  #   MAX_DOMAIN_LENGTH=50
  #   half_required_length = floor((MAX_DOMAIN_LENGTH - len(args.domain) - 1)/2)
  #   subdomain = args.subdomain[:half_required_length]
  #   subdomain += args.subdomain[-half_required_length:]
  #
  # So as an example:
  # domain=ci.stackspin.net
  # subdomain=666-fix-helmrelease-tests-in-v0-4
  # So len(args.domain) = 19
  # floor((50-20 -1)/2) = 15
  #
  # The truncated subdomain would be:
  # 666-fix-helmrel + e-tests-in-v0-4 = 666-fix-helmrele-tests-in-v0-4


  if [[ ${#id} -gt 32 ]]
  then
    left="${id:0:18}"
    right="$(echo "$id" | rev | cut -b-15 | rev)"
    id="${left}${right}"
    # echo "Truncated subdomain is: $id"
  fi

  # Test if argument is an IP or a host name
  if [[ $id =~ [0-9]{1,3}(\.[0-9]{1,3}){3} ]]
  then
    # ip addr
    host=$id
  else
    # gitlab CI replaces dots with dashes
    id=$(echo "$id" | tr '.' '-')
    host="${id}".ci.stackspin.net
  fi

  host=$(echo "$host" | tr '_/' '-')
  echo $host
}

stackspin_ci_urls () {
  host=$(stackspin_ci_branch_dnsname $*)
  echo "Grafana: https://grafana.$host"
}

cluster_env () {
  export CLUSTER_DIR=clusters/$1
  export KUBECONFIG=$CLUSTER_DIR/kube_config_cluster.yml
  kubectl get node
}
