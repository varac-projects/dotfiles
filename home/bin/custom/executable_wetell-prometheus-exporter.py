#!/usr/bin/env python3
"""Scraper für wetell.de auf Basis von Playwright.

Exportiert den Volumenverbrauch in MB des jetzigen und letzten
Monats/Abrechnungszeitraums, und die Anzahl der verbleibenen Tage.

Beispiel:

$ ./wetell-prometheus-exporter.py
wetell_inklusiv{} 30000
wetell_verbraucht{} 23427
wetell_tage_verbleibend{} 1
wetell_letzter_monat_inklusiv{von="06.12.",bis="05.01.2024"} 30000
wetell_letzter_monat_verbraucht{von="06.12.",bis="05.01.2024"} 20519

Todo:
  - Jahreszahl zu letzter_monat_bis_str hinzufügen ?

"""

import os

from playwright.sync_api import Playwright, sync_playwright


def run(playwright: Playwright) -> None:
    """Run playwright."""
    password = os.environ["WETELL_PASSWORD"]
    username = os.environ["WETELL_USER"]
    headless = os.getenv("WETELL_HEADLESS", "True").lower() in ("true", "1", "t")

    browser = playwright.firefox.launch(headless=headless)
    page = browser.new_page(base_url="https://mein.wetell.de/")
    page.goto("https://mein.wetell.de/")
    page.goto("anmelden")
    page.get_by_placeholder("Kund*innen-Nr. oder Anmeldename*").click()
    page.get_by_placeholder("Kund*innen-Nr. oder Anmeldename*").fill(username)
    page.get_by_placeholder("Passwort*").click()
    page.get_by_placeholder("Passwort*").fill(password)
    page.get_by_role("button", name="Login").click()
    page.goto("vertraege/index")

    # Find aktive contact, open it
    page.locator('td:right-of(:text("aktiv"))').first.click()

    # Parse metrics
    column = page.locator('td:right-of(:text("Inklusivvolumen"))')
    inklusiv = str(column.first.text_content()).split()[0]
    verbraucht = str(column.nth(1).text_content()).split()[0]
    tage = str(column.nth(2).text_content()).split()[1]

    letzter_monat = str(page.get_by_text("Im letzten").nth(1).text_content()).split()
    letzter_monat_von = letzter_monat[3].strip("(")
    letzter_monat_bis = letzter_monat[5].strip(")")
    letzter_monat_verbraucht = letzter_monat[7]
    letzter_monat_inklusiv = letzter_monat[10]

    print(f"wetell_inklusiv{{}} {inklusiv}")
    print(f"wetell_verbraucht{{}} {verbraucht}")
    print(f"wetell_tage_verbleibend{{}} {tage}")
    print(
        f'wetell_letzter_monat_inklusiv{{von="{letzter_monat_von}",bis="{letzter_monat_bis}"}}'
        f" {letzter_monat_inklusiv}",
    )
    print(
        f'wetell_letzter_monat_verbraucht{{von="{letzter_monat_von}",bis="{letzter_monat_bis}"}}'
        f" {letzter_monat_verbraucht}",
    )

    browser.close()


with sync_playwright() as playwright:
    run(playwright)
