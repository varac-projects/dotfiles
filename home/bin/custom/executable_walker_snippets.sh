#!/usr/bin/env bash

FILES=(~/.config/nvim/snippets/_/*.snippets ~/.config/nvim/snippets/markdown.snippets)
# echo "${FILES[@]}"

snippet=$(grep -h '^snippet' "${FILES[@]}" |
  sed 's/snippet //' |
  walker --dmenu)
sed -n "/$snippet/I,/^[^[:space:]]/ { /$snippet/I {p;d}; /^[^[:space:]]/q; p }" "${FILES[@]}" |
  sed '1d; s/^ *//' | wl-copy
