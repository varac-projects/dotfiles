#!/usr/bin/env bash

watson_project="$(watson status -p)"
if [ "${watson_project:0:3}" = 'No ' ]
then
  watson_summary='No project tracked'
else
  watson_tag="$(watson status -t)"
  watson_elapsed="$(watson status -e)"
  watson_summary="${watson_project} ${watson_tag} (${watson_elapsed})"
fi
echo $watson_summary
