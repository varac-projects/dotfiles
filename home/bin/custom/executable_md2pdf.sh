#!/usr/bin/env bash

# Requirements:
#
#   sudo pacman -S python-weasyprint pandoc
#
# Changelog:
#
# - 2024-09-29: Use weasyprint pdf engine instead of xelatex:
#               Much lighter, works fine.

usage() {
  cat <<EOF
  usage: $0 OPTIONS file [file2...]

  Generates a PDF from a/mulitple markdown files

  OPTIONS:
    -o | --out:        output PDF file (required)
    -h | --help:       Show this message
    -v | --verbose:    Be more verbose
EOF
  exit 0
}

options=$(getopt -o o:vh -l out:,verbose,help -- "$@")
eval "set -- $options"

verbose=0

while true; do
  case $1 in
    -v | --verbose)
      ((verbose++))
      shift
      ;;
    -o | --out)
      out=$2
      shift 2
      ;;
    -h | --help)
      usage
      ;;
    --)
      shift
      break
      ;;
    *) exit 1 ;; # error
  esac
done

src=("$@")
[[ -n "${src[0]}" ]] || usage
[[ -n $out ]] || usage

if [[ $verbose -gt 0 ]]; then
  echo "src: ${src[*]}"
  echo "out: $out"
fi

# Optional pandoc options:
#
# `-V fontsize=12pt` only sets the fontsize of normal text, not in code blocks
# (this is done in ~/hledger/report-pandoc.yml)
# --from=markdown+smart   needed for working \newpage

# -V fontsize=12pt \
# -V geometry:"top=2cm, bottom=1.5cm, left=2cm, right=1.5cm" \

# Only for xelatex pdf engine:
# --include-in-header ~/projects/markup/markdown/pandoc/pdf-defaults.tex \

pandoc \
  --pdf-engine=weasyprint \
  --pdf-engine-opt=--stylesheet=/home/varac/.config/weasyprint/md2pdf.css \
  --from=gfm+smart+hard_line_breaks \
  -o "$out" \
  -s "${src[@]}"
