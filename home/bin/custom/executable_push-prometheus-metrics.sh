#!/bin/bash
#
# Scapes multiple scrape targets and pushes gathered metrics to a prometheus pushgateway
# Supports basic auth for the pushgateway
#
# Config env variables:
#
# SCRAPE_TARGETS: One or multiple scrape target configs in the form of
#   "scrape_target_url;hostname;bearer_token"
#
#   Multiple scrape target configs should be seperated by a whitespace
#   Defaults to "http://localhost:9100/metrics" (node-exporter)
# PUSHGATEWAY_URL: URL of the push gateway
# PUSHGATEWAY_USER: Basic auth username
# PUSHGATEWAY_PW: Basic auth password
#
# Usage:
#
#   ./push-prometheus-metrics
#
# TODO:
# * remove backslashes from regex
# * remove comments from push

set -Eeuo pipefail

function exit0 {
  echo "sth went wrong, exiting gracefully..."
  exit 0
}

trap exit0 ERR

BASE_URL="${PUSHGATEWAY_URL}/metrics/job/node-exporter/instance"
targets="${SCRAPE_TARGETS:-http://localhost:9100/metrics;$HOSTNAME}"
# Split into array (https://www.shellcheck.net/wiki/SC2206)
IFS=" " read -r -a targets <<<"$targets"

for target in "${targets[@]}"; do
  IFS=";" read -r -a target_array <<<"$target"
  url=${target_array[0]}
  target_name=${target_array[1]}
  bearer=${target_array[2]:-''}
  push_url="${BASE_URL}/${target_name}"
  echo "Scraping $url and pushing metrics"
  echo "for host \"$HOSTNAME\" to ${push_url}"
  # Filter out unimportant metrics before pushing to pushgateway
  curl -H "Authorization: Bearer $bearer" -s "$url" |
    grep -v "\(\(^\| \)go_\|http_request\|http_requests\|http_response\|process_\)" |
    curl --no-progress-meter -u "$PUSHGATEWAY_USER:$PUSHGATEWAY_PW" --data-binary @- "$push_url"
done
