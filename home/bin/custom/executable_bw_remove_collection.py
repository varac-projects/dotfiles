#!/usr/bin/env python3
"""BitWarden tool to delete collections using bw CLI tool.

You must install the bitwarden CLI tool (https://bitwarden.com/help/article/cli/#download-and-install)
Create your session as per the instructions on the download page.
You can replace your session key and use the following to process the results.

Taken from https://gist.github.com/eduncan911/c0f4d789b87c3746a055779a57ff57e5

Usage:

    bw_remove_collection.py <root_collection ID>

"""

import json
import os
import sys

import sh

session_key = os.environ["BW_SESSION"]
root_collection = sys.argv[1]


# Sets this instance to use the provided session.
bw = sh.bw.bake("--session", session_key)  # type: ignore[reportAttributeAccessIssue]

collections = bw("list", "collections")

# The output provided is a string, luckily we can convert this to JSON
collections = json.loads(collections.stdout)

print(collections)

# Delete all the collections.. Add logic here if you want only a subset.
for collection in collections:
    if collection.get("id") and collection.get("name").startswith(root_collection):
        print(f"Deleting {collection[id]}")
        bw(
            "delete",
            "org-collection",
            collection["id"],
            "--organizationid",
            collection["organizationId"],
        )
