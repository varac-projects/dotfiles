#!/usr/bin/env bash

shopt -s expand_aliases

echo "Documented key bindings: "
sway_config | grep '^## ' | sed 's/## //; s| // |;|g; s/ ##/;/; s/; /;/' | sort | csv2md -d';'
