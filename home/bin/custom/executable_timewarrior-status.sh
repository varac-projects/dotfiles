#!/usr/bin/env bash
# Taken from https://github.com/kohane27/Waybar-Clockify/blob/main/src/Waybar-Clockify/main.sh

if [[ "$(timew get dom.active)" == "0" ]]; then
  printf "%s\n" "-"
else
  current=$(timew)
  if [[ "$current" =~ (Total.*?[0-9]{1}:[0-9]{2}:[0-9]{2}) ]]; then
    tags=$(echo "$current" | head -1 | cut -d" " -f2-)
    total_raw=$(echo "${BASH_REMATCH[1]}" | cut -c 20-24 | tr ':' 'h')
    # add 0 to hours worked
    total="0${total_raw}"
    echo "$tags ${total: -5}"
  fi
fi
