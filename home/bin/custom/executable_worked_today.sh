#!/bin/sh
# Compares hours worked today reported by Watson and Wakatime

echo "Time worked today:"
echo "Watson:   $(watson report -d | grep Total | sed 's/^Total: //')"
echo "Wakatime: $(wakatime --today)"
