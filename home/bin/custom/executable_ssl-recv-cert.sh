#!/bin/sh
# https://serverfault.com/questions/139728/how-to-download-the-ssl-certificate-from-a-website

DOMAIN=$1
PORT=$2

gnutls-cli "$DOMAIN" -p "$PORT" --insecure --print-cert < /dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'
