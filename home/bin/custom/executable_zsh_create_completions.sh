#!/usr/bin/env bash
#
# Generate zsh completion for tools that doesn't package it
#
# Todo:
#   * configure systemd-timer to run script on a regular base

set -euo pipefail

COMPDIR=~/.config/shell/zsh/completion

# Bring git repos with completion files up to date
for dir in /home/varac/projects/timetracking/timewarrior-zsh-plugin ~/projects/containers/libpod; do
  if [ -d "$dir" ]; then
    echo "Pulling $dir..."
    cd "$dir"
    git pull --rebase
  else
    echo "$dir not found, skipping"
  fi
done

# Integrated completion script output
for app in cmctl; do
  if command -v "$app" &>/dev/null; then
    echo "Installing $COMPDIR/_$app"
    "$app" completion zsh >"$COMPDIR/_$app"
  else
    echo "$app is not installed"
  fi
done

# Diverse tools without packaged zsh completion

logcli --completion-script-zsh >"$COMPDIR"/_logcli

wget -q https://raw.githubusercontent.com/go-task/task/master/completion/zsh/_task -O "$COMPDIR"/_task
wget -q https://raw.githubusercontent.com/ianmkenney/timewarrior_zsh_completion/main/_timew -O "$COMPDIR"/_timew

ln -s /usr/share/zsh/site-functions/tenv "$COMPDIR"/_tenv
