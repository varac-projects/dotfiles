#!/user/bin/env bash
#
# https://www.reddit.com/r/selfhosted/comments/rzzn3q/cronjob_for_updating_transmissions_peerport_to/

HOSTNAME=transmission
HOSTNAME=localhost
RPC_PORT=9091
HOST="$HOSTNAME:$RPC_PORT"

# Get the x-transmission-session-id
id=$(curl -Ssl -i http://$HOST/transmission/rpc | grep "^X-Transmission-Session-Id:" | sed -n "s/X-Transmission-Session-Id: //p" | grep -o '[[:alnum:]]*')
echo "Session ID: $id"

port=$(cat /tmp/gluetun/forwarded_port)
echo "Forwarded port: $port"

payload="{\"arguments\":{\"peer-port\":$port},\"method\":\"session-set\"}"
echo "Payload: $payload"

curl -Ssl -v -H "Content-Type: application/json" \
  -d "$payload" \
  --header "x-transmission-session-id: $id" \
  http://$HOST/transmission/rpc
