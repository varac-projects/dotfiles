#!/usr/bin/env bash
# Exports last snapshot timestamps to prometheus metrics, using s3 backend

set -euo pipefail

MINIO_ALIAS="${MINIO_ALIAS:-restic_exporter}"

log() {
  # /dev/log doesn't exist in a kubernetes pod by default
  if [ -S /dev/log ]; then
    logger -s "$@"
  else
    echo -e "$@" >/dev/stderr
  fi
}

get_metrics() {
  # Get metrics for bucket/path combination. Exports:
  #   - Size of path
  #   - Timestamp of last restic snapshot
  path=$1

  bucket=${path%%/*}
  dir=$(basename "$path")
  if [ "$bucket" == "$dir" ]; then
    dir=""
  fi
  fullpath="${MINIO_ALIAS}/${path}"

  # Disk usage
  du=$(mc du --json "$fullpath")
  size=$(echo "$du" | jq .size)
  objects=$(echo "$du" | jq .objects)
  echo "restic_server_dir_size{bucket=\"$bucket\", dir=\"$dir\", path=\"$path\"} $size"
  echo "restic_server_dir_objects{bucket=\"$bucket\", dir=\"$dir\", path=\"$path\"} $objects"

  # Timestamp of last backup
  snapshots=$(mc find --json "${fullpath%/}/snapshots" || true)
  timestamp=$(echo "$snapshots" | jq -s -r 'sort_by(.lastModified)|[last][0].lastModified' || true)
  if [ "$timestamp" = "null" ]; then
    latest_ts=0
  else
    timestamp=$(echo "$snapshots" | jq -s -r 'sort_by(.lastModified)|[last][0].lastModified')
    latest_ts=$(date "+%s" -d "$timestamp")
  fi
  echo "restic_server_last_snapshot_ts{bucket=\"$bucket\", dir=\"$dir\", path=\"$path\"} $latest_ts"
}

get_restic_dirs() {
  root_dir="$1"
  # If exclude str is not passed, default to sth that would not match any dir
  exclude="${2:-dhwuizhrewuh}"
  mapfile -t dirs < <(mc ls --json "${MINIO_ALIAS}/${root_dir}" | jq -r '.key' | sed 's|/.*||' | grep -v "$exclude" | sed "s|^|${root_dir}/|" | sed 's|^/||')
  echo "${dirs[@]}" | tr -d '\n'
}

# Main
######

# Gather bucket dirs
mapfile -d" " -t restic_dirs < <(get_restic_dirs '' '^velero$')
mapfile -d" " -t -O "${#restic_dirs[@]}" restic_dirs < <(get_restic_dirs 'velero/k.varac.net/restic')
mapfile -d" " -t -O "${#restic_dirs[@]}" restic_dirs < <(get_restic_dirs 'velero/media.varac.net/restic')

log "Gathered these restic locations: ${restic_dirs[*]}\n"

for dir in "${restic_dirs[@]}"; do
  log "Getting stats for dir: $dir"
  get_metrics "$dir"
done

log "Successfully exported all restic server metrics."
