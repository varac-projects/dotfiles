#!/bin/sh
set -e

path="$ANSIBLE_VAULT_IDENTITY"

rbw list | grep -q "^${path}$" ||
  (
    echo 'Cant find password, is ANSIBLE_VAULT_IDENTITY exported ? Exiting.'
    exit 1
  )

rbw get "$path"
