#!/usr/bin/env python3

"""
BitWarden tool to delete folders using bw CLI tool

You must install the bitwarden CLI tool (https://bitwarden.com/help/article/cli/#download-and-install)
Create your session as per the instructions on the download page.
You can replace your session key and use the following to process the results.

Taken from https://gist.github.com/voice1/58159602be2d9ef6cdb72b8c2224ca72
"""

import sh
import json
import os
import sys

session_key = os.environ["BW_SESSION"]
root_folder = sys.argv[1]

print(f'Removing folder "{root_folder}" and all sub-folders')

# Sets this instance to use the provided session.
bw = sh.bw.bake("--session", session_key)  # type: ignore

folders = bw("list", "folders")

# The output provided is a string, luckily we can convert this to JSON
folders = json.loads(folders.stdout)

# Delete all the folders.. Add logic here if you want only a subset.
for folder in folders:
    if folder.get("id") and folder.get("name").startswith(root_folder):
        print(f"removing folder {folder['id']} {folder['name']}")
        bw("delete", "folder", folder["id"])
