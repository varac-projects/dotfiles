#!/usr/bin/env sh
#
# Lists recently used file:/// locations,
#
# Prerequisites:
#   - https://kislyuk.github.io/yq/#xq
#   - https://github.com/AquilaIrreale/urlencode
#     - AUR package: `urlencode`

/home/varac/.local/bin/xq \
  -j '.xbel.bookmark[] | ."@modified", ",", ."@href", "\n"' \
  ~/.local/share/recently-used.xbel |
  urlencode -d |
  sort |             # sort by `modified` date
  cut -d ',' -f 2 |  # Remove date column
  sed '/^file:/!d' | # Filter out anything else like `smb://`, `mtp://`, `sftp://`
  sed 's|^file://||' # Remove leading `file://`
