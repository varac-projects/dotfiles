#!/bin/bash
# https://www.jvt.me/posts/2021/12/10/bluetooth-percent-linux/

macs=$(bluetoothctl devices Connected | cut -d" " -f2)

for mac in $(echo "$macs")
do
  name=$(bluetoothctl info $mac | grep 'Name: ' | sed 's/.*Name: *//')
  mac="${mac//:/_}"
  percent=$(dbus-send --print-reply=literal --system --dest=org.bluez /org/bluez/hci0/dev_${mac} org.freedesktop.DBus.Properties.Get string:"org.bluez.Battery1" string:"Percentage" | sed 's/.*byte //')
  echo "Power level for $name ($mac): $percent%"
done
