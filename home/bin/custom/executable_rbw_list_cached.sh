#!/bin/sh
# Produce a list of all Bitwarden items, cahced for a certain
# amount of seconds. The goal is to speed up password selection
# with walker

cache_file="/tmp/rbw.list"
cache_seconds=60

write_file() {
  rbw list --fields name,user | tr '\t' ' ' >$cache_file
  chmod 600 $cache_file
}

if [ -f $cache_file ]; then
  age=$(($(date +%s) - $(stat --format=%Y "$cache_file")))
  if [ $age -gt $cache_seconds ]; then
    write_file
  fi
else
  write_file
fi
cat $cache_file
