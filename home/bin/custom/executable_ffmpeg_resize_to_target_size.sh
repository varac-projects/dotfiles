#!/usr/bin/env bash
#
# see https://unix.stackexchange.com/a/598360
#
# Example usage to produce a ~1 GB file:
#
#   ffmpeg_resize_to_target_size.sh source.mp4 1024

path=$1
# https://stackoverflow.com/a/965072
path_without_exttion="${path%.*}"
extention="${path##*.}"

target_size_mb=$2                                 # target size in MB
target_size=$((target_size_mb * 1000 * 1000 * 8)) # target size in bits
length=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$path")
length_round_up=$((${length%.*} + 1))
total_bitrate=$((target_size / length_round_up))
audio_bitrate=$((128 * 1000)) # 128k bit rate
video_bitrate=$((total_bitrate - audio_bitrate))

# -scodec copy is needed to preserve subtitles, see https://stackoverflow.com/a/27671819
ffmpeg \
  -i "$path" \
  -b:v "$video_bitrate" \
  -maxrate:v "$video_bitrate" \
  -bufsize:v $((target_size / 20)) \
  -b:a "$audio_bitrate" \
  -scodec copy \
  "${path_without_exttion}-${target_size_mb}mb.${extention}"
