#!/bin/sh
# Exports email addresses from the notmuch database to
# notmuch, CSV and JSON format
# Taken and adapted from https://gitlab.com/anarcat/scripts/-/blob/main/notmuch-address?ref_type=heads

# Filter and export addresses in notmuch format
#
# - look for sent *and* received mail, then restrict to our search
#   pattern, otherwise `--output=recipients` also includes unrelated
#   recipients.
# - filter out all results from certain lists
# - Remove ASCII escaped single quotes (`&#39;`)
# - remove quotes around the name
# - use `uniq` instead of `sort -u` because we hope to
#   keep the `newest-first` sort order provided by notmuch.

BASENAME="$HOME/.config/notmuch/addresses"

echo "Exporting notmuch addresses in notmuch format to ${BASENAME}.orig"
(
  notmuch address --output=recipients to:"$*"
  notmuch address from:"$*"
) >"${BASENAME}.orig"

echo "Filtering and cleaning notmuch addresses in notmuch format to $BASENAME"
grep -Ev "(noreply.github.com|members.ebay.com.hk|googlegroups.com>)" \
  "${BASENAME}".orig |
  sed -e 's/&#39;//g' \
    -e "s/^[\"']*//" \
    -e "s/[\"']* </ </" |
  grep "$*" |
  uniq >"${BASENAME}"

echo "Converting notmuch addresses to CSV format to ${BASENAME}.csv"
echo "name;address" >"${BASENAME}".csv

# - convert to csv
# - add `;` to the beginning for entries with only an email addr
sed \
  -e 's/ </;/; s/>//' \
  -e '/;/!s/^/;/' \
  "${BASENAME}" >>"${BASENAME}.csv"

echo "Converting notmuch addresses to JSON format to ${BASENAME}.json"
csvjson --blanks -d ';' "${BASENAME}".csv >"${BASENAME}".json
