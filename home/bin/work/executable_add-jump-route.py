#!/usr/bin/env python3
"""Add a route to a jumphost for AutoSSH + TinyProxy.

This allows bypassing corporate proxies / VPNs.

Requirements:
=============
* https://pyroute2.org/
* https://pypi.org/project/pyroute2/

Example routes
==============

Without VPN
-----------

default via 10.27.13.1 dev wlan0 proto dhcp src 10.27.13.151 metric 600
10.27.13.0/24 dev wlan0 proto kernel scope link src 10.27.13.151 metric 600
172.17.0.0/16 dev docker0 proto kernel scope link src 172.17.0.1 linkdown

With VPN enabled
----------------

default via 192.168.224.53 dev tun0 metric 1
1.1.1.1 dev tun0 proto kernel scope link src 192.168.224.53
10.27.13.0/24 dev wlan0 proto kernel scope link src 10.27.13.180 metric 600
10.27.13.1 via 10.27.13.180 dev wlan0 proto unspec metric 1
172.17.0.0/16 dev docker0 proto kernel scope link src 172.17.0.1 linkdown
194.113.141.95 via 10.27.13.1 dev wlan0 src 10.27.13.180 metric 1
"""

import socket
import struct
import subprocess
from pathlib import Path

from pyroute2 import IPRoute
from pyroute2.netlink.exceptions import NetlinkError


def route(action: str, dst: str, gateway: str) -> None:
    """Add/del/modify route.

    https://docs.pyroute2.org/iproute.html#pyroute2.iproute.linux.RTNL_API.route
    """
    ipr = IPRoute()

    if action == "add":
        # replace creates a new one, if there is no such route yet
        print(f"Adding route dst:{dst} gateway:{gateway}")
        ipr.route("replace", dst=dst, gateway=gateway)

    if action == "del":
        try:
            print(f"Removing route dst:{dst} gateway:{gateway}")
            ipr.route("del", dst=dst, gateway=gateway)
            print(f"Route {dst} {gateway} is present in routing table, removing")
        except NetlinkError:
            print(
                f"Route {dst} {gateway} is not present in routing table, doing nothing"
            )


def get_default_gateways() -> dict:
    """Read the default gateways directly from /proc."""
    gateways = {}
    with Path("/proc/net/route").open() as fh:
        for line in fh:
            fields = line.strip().split()
            if fields[1] != "00000000" or not int(fields[3], 16) & 2:
                # If not default route or not RTF_GATEWAY, skip it
                continue

            ip = socket.inet_ntoa(struct.pack("<L", int(fields[2], 16)))
            metric = fields[6]
            gateways[ip] = metric

    return gateways


def get_interface_names() -> list:
    """Return list of network interface names."""
    if_names = socket.if_nameindex()
    return list(list(zip(*if_names))[1])


def main() -> None:
    """Run main function."""
    # bitrigger.varac.net
    jump_adr = "188.94.25.109"
    if_names = get_interface_names()

    if "tun0" in if_names:
        print("Interface tun0 is up, F5 VPN is setup.")
        non_vpn_gateway = subprocess.check_output(
            # The `jump_adr` default route is removed
            # immediately after starting f5
            # (and eventually re-added minutes later), so we need to jump through
            # some hoops here to find the non-f5 default gateway
            '/usr/sbin/ip -j route list | jq -r \'.[] | select(.protocol=="unspec") | '
            'select(.dev!="tun0") | .dst\'',
            shell=True,  # noqa: S602
            universal_newlines=True,
        ).strip()
        route("add", dst=jump_adr, gateway=non_vpn_gateway)
    else:
        print("Interface tun0 is not up, F5 VPN is not setup.")
        default_gws = get_default_gateways()
        default_gw_count = len(default_gws)
        if default_gw_count != 1:
            print("More or less than 1 default gateway found, don't know what to do !")
            raise
        non_vpn_gateway = next(iter(default_gws))
        route("add", dst=jump_adr, gateway=non_vpn_gateway)


if __name__ == "__main__":
    main()
