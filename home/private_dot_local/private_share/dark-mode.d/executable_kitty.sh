#!/usr/bin/env sh

kitten themes --reload-in=all Gruvbox\ Dark
sed -Ei 's/Gruvbox (Material Dark Hard|Dark|Light)/Gruvbox/' ~/.config/kitty/kitty.conf
