# Varac's dotfiles

Managed with [chezmoi](https://www.chezmoi.io/).

See also the
[dotfiles](https://0xacab.org/varac-projects/doc/-/blob/main/docs/etc/dotfiles.md) documentation.
